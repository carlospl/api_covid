#!/bin/bash

for i in $(cat dades2.csv)
do
fecha=$(echo $i | cut -d';' -f1)
casos=$(echo $i | cut -d';' -f3)
muertos=$(echo $i | cut -d';' -f7)
ia14=$(echo $i | cut -d';' -f5)
ia7=$(echo $i | cut -d';' -f6)

if [ -z $fecha ]
then
	fecha=0
fi
if [ -z $ia14 ]
then
	ia14=0
fi
if [ -z $ia7 ]
then
	ia7=0
fi
if [ -z $muertos ]
then
        muertos=0
fi
if [ -z $casos ]
then
       casos=0
fi

echo "INSERT INTO ia14 (fecha,id_ccaa,incidencia) VALUES ('$fecha',1,$ia14);"
echo "INSERT INTO ia7 (fecha,id_ccaa,incidencia) VALUES ('$fecha',1,$ia7);"
echo "INSERT INTO casos (fecha,id_ccaa,numero) VALUES ('$fecha',1,$casos);"
echo "INSERT INTO muertos (fecha,id_ccaa,numero) VALUES ('$fecha',1,$muertos);"




done
