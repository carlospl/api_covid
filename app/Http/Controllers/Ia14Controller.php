<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ia14;

class Ia14Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $ia14 = new ia14();
        $ia14->fecha=$request->fecha;
        $ia14->id_ccaa=$request->id_ccaa;
        $ia14->incidencia=$request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }
    public function showAll()
    {

        $ia14 = ia14::all();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia14],200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ia14=DB::select(DB::raw("select * from ia14 where fecha='$id'"));
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra la fecha'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$ia14],200);
    }

    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $ia14 = DB::select(DB::raw("select * from ia14 where fecha BETWEEN '$id1' and '$id2' "));

        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($ia14);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$id2,$incidencia)
    {
        $cambio= DB::update(DB::raw("update ia14 set fecha = '$id2',incidencia='$incidencia' where  id='$id'"));
        if (!$cambio)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$cambio],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
