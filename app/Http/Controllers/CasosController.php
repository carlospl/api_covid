<?php

namespace App\Http\Controllers;
use App\Http\Resources\CovidCollection;
use App\Models\casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $casos = new \Casos();
        $casos->fecha=$request->fecha;
        $casos->id_ccaa=$request->id_ccaa;
        $casos->incidencia=$request->incidencia;
        $casos->save();
        return response()->json($casos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $casos = DB::select(DB::raw("select * from casos where fecha='$id'"));
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    public function showAll()
    {

        $casos = casos::all();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$casos],200);
    }
    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $casos = DB::select(DB::raw("select * from casos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($casos);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$id2,$numero)
    {
        $cambio= DB::update(DB::raw("update casos set fecha = '$id2',numero='$numero' where  id='$id'"));
        if (!$cambio)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$cambio],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
